import psycopg2
from time import time
from base import Session, engine, Base
from entities import Audience, Participant,  Conference, Phone

def convertStrToType(type_name):
    if type_name == "phones": return Phone
    elif type_name == "audiences": return Audience
    elif type_name == "conferences": return Conference
    elif type_name == "participants": return Participant
    else: return None

class Model(object):

    def __init__(self, dbname):
        self.database_name = dbname
        self.connection = None
        self.cursor = None
        Base.metadata.create_all(engine)
        self.session = None

#---------------CONNEECTING METHODS---------------
    def connect(self):
        try:
            self.connection = psycopg2.connect(user = "postgres",password = "miahvbl0276",
            host = "127.0.0.1", port="5432",database=self.database_name)
            self.cursor = self.connection.cursor()
            self.session = Session()
        except(Exception, psycopg2.Error) as error:
            print("Connection Error!", error)

    def close(self):
        if self.connection:
            self.cursor.close()
            self.connection.close()
            self.session.close()

    def reconnect(self):
        self.close()
        self.connect()

#---------------GENERAL METHODS---------------
    def get_items(self, table, value):
        try:
            return self.session.query(convertStrToType(table)).get(value)
        except: 
            print ("Error: wrong value type!")
            self.reconnect()

    def get_link(self, table, column1, value1, column2, value2):
        try:
            self.cursor.execute("SELECT * FROM {} WHERE {} = '{}' AND {} = '{}'"
                                .format(table, column1, value1, column2, value2))
            self.connection.commit()
            return self.cursor.fetchall()
        except:
            print ("Error: wrong value type!")
            self.reconnect()

    def delete_items(self, item):
        try:
            self.session.delete(item)
            self.session.commit()
            return item
        except:
            print("Delete Error: wrong value type!")
            self.reconnect()

    def delete_link(self, table1, value1, table2, value2):
        try:
            conf = self.session.query(convertStrToType(table1)).get(value1)
            part = self.session.query(convertStrToType(table2)).get(value2)
            del conf.participants[conf.participants.index(part)]
            self.session.commit()
        except:
            print("Delete Error: wrong value type!")
            self.reconnect()

#---------------CREATING METHODS---------------
    def create_participant(self, name, surname, age):
        new_participant = Participant(name, surname, age)
        self.session.add(new_participant)
        self.session.commit()
        return new_participant.id

    def create_audience(self, num, corps_num):
        new_audience = Audience(num, corps_num)
        self.session.add(new_audience)
        self.session.commit()
        return new_audience.id

    def create_conference(self, date, topic, audience):
        new_conference = Conference(date, topic, audience)
        self.session.add(new_conference)
        self.session.commit()
        return new_conference.id

    def create_phone(self, num, part):
        try:
            self.session.add(Phone(num, part))
            self.session.commit()
        except: self.reconnect(); raise

    def create_pc_link(self, conf, part):
        try:
            conf.participants.append(part)
            self.session.commit()
        except: self.reconnect()

#---------------UPDATING METHODS---------------
    def update_participant(self, participant, name, surname, age):
        participant.name = name
        participant.surname = surname
        participant.age = age
        self.session.commit()

    def update_audience(self, audience, audience_number, corps_number):
        audience.audience_number = audience_number
        audience.corps_number = corps_number
        self.session.commit()

    def update_conference(self, conference, conference_date, conference_topic, audience):
        conference.conference_date = conference_date
        conference.conference_topic = conference_topic
        conference.audience = audience
        self.session.commit()

    def update_phone(self, phone, participant):
        try:
            phone.participant = participant
            self.session.commit()
        except: self.reconnect()

#---------------GENERATING METHODS---------------
    def generate_audiences(self, num):
        self.cursor.execute("INSERT INTO audiences (audience_number, corps_number)"
	                        " SELECT num, corps FROM"
	                        " (SELECT trunc(1+random()*600)::int AS num, trunc(1+random()*20)::int"
                            " AS corps FROM generate_series(1,{}))"
                            " AS auds LEFT JOIN audiences ON num = audience_number AND corps = corps_number"
                            " WHERE audience_number IS null".format(num))
        self.connection.commit()

    def generate_participants(self, num):
        self.cursor.execute("INSERT INTO participants (name, surname, age)"
                            " SELECT chr(trunc(65+random()*25)::int) || random_str(3+(random()*5)::int),"
                            " chr(trunc(65+random()*25)::int) || random_str(4+(random()*6)::int),"
                            " (18+(random()*10)::int) FROM generate_series(1,{})".format(num))
        self.connection.commit()

    def generate_conferences(self, num):
        self.cursor.execute("INSERT INTO conferences (conference_date, conference_topic, audience_id)"
                            " SELECT random_date(), random_str(6 + (random()*10)::int), random_audience()"
                            " FROM generate_series(1, {})".format(num))
        self.connection.commit()


    def generate_phones(self, num):
        self.cursor.execute("INSERT INTO phones (phone, participant_id)"
                            " SELECT num, p_id FROM"
                            " (SELECT random_participant() AS p_id, num FROM"
                            " (SELECT random_phone_ua() AS num FROM generate_series(1,{}) GROUP BY num) AS phns) AS gen_phns"
                            " LEFT JOIN phones ON num = phone WHERE phone IS null".format(num))
        self.connection.commit()

    def generate_links(self, num):
        self.cursor.execute("INSERT INTO conference_participant (conference_id, participant_id)"
                            " WITH glinks AS (SELECT random_conference() AS cid, random_participant() AS pid"
                            " FROM generate_series(1, {}) GROUP BY cid, pid)"
                            " SELECT cid, pid FROM glinks LEFT JOIN conference_participant"
                            " ON glinks.cid = conference_id AND glinks.pid = participant_id"
                            " WHERE conference_id is null".format(num))
        self.connection.commit()

#---------------FINDING METHODS---------------
    def query1(self, c_id, age_min, count_phone_min):
        start_time = time()
        self.cursor.execute("with ids as( with cph as (select participant_id as p_id, count(participant_id)"
            " from participants inner join phones on participant_id = id"
            " group by participant_id)"
            " select p_id from cph where count >= {})"
            " select name, surname from("
            " select name, surname, id from conference_participant inner join participants"
            " on id = participant_id where conference_id = {} and age >= {}) as ps inner join"
            " ids on p_id = id".format(count_phone_min, c_id, age_min))
        self.connection.commit()
        execution_time = time() - start_time
        print("\nTime: {}\n".format(execution_time))
        return self.cursor.fetchall()

    def query2(self, c_id):
        start_time = time()
        self. cursor.execute("select sum(age)/count(conference_id) from participants inner join"
                            " conference_participant on participant_id = id where"
                            " conference_id = {} group by conference_id".format(c_id))
        self.connection.commit()
        execution_time = time() - start_time
        print("\nTime: {}\n".format(execution_time))
        return self.cursor.fetchall()

    def query3(self, corp, first_date, second_date):
        start_time = time()
        self. cursor.execute("select conference_topic from conferences inner join"
                            " (select id from audiences where corps_number = {}) as ids"
                            " on  ids.id = audience_id where conference_date > '{}'"
                            " and conference_date < '{}'".format(corp, first_date, second_date))
        self.connection.commit()
        execution_time = time() - start_time
        print("\nTime: {}\n".format(execution_time))
        return self.cursor.fetchall()
