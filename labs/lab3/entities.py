from sqlalchemy import Column, String, Integer, Date, Table, ForeignKey
from sqlalchemy.orm import relationship, backref
from base import Base

class Audience(Base):
    __tablename__ = 'audiences'

    id = Column(Integer, primary_key=True)
    audience_number = Column(Integer)
    corps_number = Column(Integer)
    conferences = relationship("Conference", back_populates='audience',
                        cascade="all, delete, delete-orphan")

    def __init__(self, num, corp):
        self.audience_number = num
        self.corps_number = corp

conference_participant_association = Table(
    'conference_participant', Base.metadata,
    Column('conference_id', Integer, ForeignKey('conferences.id')),
    Column('participant_id', Integer, ForeignKey('participants.id'))
)

class Participant(Base):
    __tablename__ = 'participants'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    surname = Column(String)
    age = Column(Integer)
    phones = relationship("Phone", back_populates='participant', cascade="all, delete, delete-orphan")
    conferences = relationship("Conference", secondary=conference_participant_association)

    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.age = age

class Conference(Base):
    __tablename__ = 'conferences'

    id = Column(Integer, primary_key=True)
    conference_date = Column(Date)
    conference_topic = Column(String)
    audience_id = Column(Integer, ForeignKey('audiences.id'))
    audience = relationship("Audience", back_populates='conferences')
    participants = relationship("Participant", secondary=conference_participant_association)

    def __init__(self, date, topic, audience):
        self.conference_date = date
        self.conference_topic = topic
        self.audience = audience

class Phone(Base):
    __tablename__ = 'phones'

    phone = Column(String, primary_key=True)
    participant_id = Column(Integer, ForeignKey('participants.id'))
    participant = relationship("Participant", back_populates='phones')

    def __init__(self, phone, participant):
        self.phone = phone
        self.participant = participant
