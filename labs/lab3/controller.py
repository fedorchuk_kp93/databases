from model import Model
from view import View

class Controller(object):

    def __init__(self, dbname):
        self.view = View()
        self.model = Model(dbname)
        self.model.connect()

    def __del__(self):
        self.model.close()

#---------------GET FROM TABLE---------------
    def get_item_by_id(self, table, id):
        res = self.model.get_items(table, id)
        if res: return res
        else: self.view.failed_get(table, id)

    def get_phone(self, phone):
        try:
            int(phone)
            if len(phone) == 10:
                res = self.model.get_items("phones", ("+38" + phone)) 
                if res: return res
                else: self.view.failed_get("phone", ("+38" + phone))
            else: print("Wrong telephone number length")
        except: print("Wrong data type!")

#---------------DELETE FROM TABLE---------------
    def delete_audience(self, a_id):
        aud = self.model.get_items("audiences", a_id)
        if aud:
            self.model.delete_items(aud)
            self.view.successful_deleting("Audience", a_id)
        else: self.view.failed_get("audience", a_id)

    def delete_participant(self, p_id):
        part = self.model.get_items("participants", p_id)
        if part:
            self.model.delete_items(part)
            self.view.successful_deleting("Participant", p_id)
        else: self.view.failed_get("participant", p_id)

    def delete_conference(self, c_id):
        conf = self.model.get_items("conferences", c_id)
        if conf:
            self.model.delete_items(conf)
            self.view.successful_deleting("Conference", c_id)
        else: self.view.failed_get("conference", c_id)

    def delete_phone(self, phone):
        try:
            int(phone)
            if len(phone) == 10:
                phon = self.model.get_items("phones", ("+38" + phone))
                if phon:
                    self.model.delete_items(phon) 
                    print("Phone {} was successfuly deleted!".format(("+38" + phone)))
                else: self.view.failed_get("phone", ("+38" + phone))
            else: print("Wrong telephone number length")
        except: print("Wrong data type!")

    def leave_conference(self, c_id, p_id):
        link = self.model.get_link("conference_participant", "conference_id", c_id, "participant_id", p_id)
        if link:
            self.model.delete_link("conferences", c_id, "participants", p_id)
            print ("Link was successfuly deleted!")
        else: print("This link doesn't exist")

#---------------ADD TO TABLE---------------
    def add_participant(self, name, surname, age):
        try:
            age = int(age)
            if len(name) and len(surname):
                if age >= 18 and age < 90:
                    newid = self.model.create_participant(name, surname, age)
                    self.view.successful_creating("participant", newid)
                    return newid
                else:
                    print("Age out of range!")
            else:
                print("Empty values!")
        except: print("Wrong data type!")

    def add_audience(self, num, corps_num):
        try:
            num = int(num); corps_num = int(corps_num)
            if num > 0 and corps_num > 0:
                newid = self.model.create_audience(num, corps_num)
                self.view.successful_creating("audience", newid)
                return newid
            else:
                print("Bad values!")
        except: print("Wrong data type!")

    def add_conference(self, date, topic, a_id):
        au = self.model.get_items("audiences", a_id)
        if au:
            if len(topic) > 0:
                newid = self.model.create_conference(date, topic, au)
                self.view.successful_creating("conference", newid)
                return newid
            else:
                print("Empty topic!")
        else: print("No audience with {} id!".format(a_id))

    def add_phone(self, phone, p_id):
        try:
            int(phone); p_id = int(p_id)
            if len(phone) == 10:
                part = self.model.get_items("participants", p_id)
                if part:
                    try:
                        self.model.create_phone(("+38" + phone), part)
                        print("New phone was successfuly added!")
                    except: print("A telephone with this number already exist!")
                else: self.view.failed_get("participant", p_id)
            else: print("Wrong telephone number length")
        except: print("Wrong data type!")

    def join_participant(self, c_id, p_id):
        part = self.model.get_items("participants", p_id)
        conf = self.model.get_items("conferences", c_id)
        if conf and part:
            try:
                self.model.create_pc_link(conf, part)
            except: print("Error this link just exist")

#---------------UPDATE IN TABLE---------------
    def update_audience(self, id, num, corp):
        try:
            aud = self.model.get_items("audiences", id)
            if aud:
                int(num), int(corp)
                if num > 0 and corp > 0:
                    self.model.update_audience(aud, num, corp)
                    self.view.successful_updating("audience")
                else: print("Bad values!")
            else: self.view.failed_get("audiences", id)
        except: print("Wrong data type!")

    def update_participant(self, id, name, surname, age):
        try:
            part = self.model.get_items("participants", id)
            if part:
                age = int(age)
                if len(name) and len(surname):
                    if age >= 18 and age < 90:
                        self.model.update_participant(part, name, surname, age)
                        self.view.successful_updating("participant")
                    else: print("Age out of range!")
                else: print("Empty values!")
            else: self.view.failed_get("participants", id)
        except: print("Wrong data type!")
    
    def update_conference(self, id, date, topic, a_id):
        try:
            conf = self.model.get_items("conferences", id)
            if conf:
                au = self.model.get_items("audiences", a_id)
                if au:
                    if len(topic) > 0:
                        self.model.update_conference(conf, date, topic, au)
                        self.view.successful_updating("conference")
                    else:
                        print("Empty topic!")
                else: self.view.failed_get("audience", a_id)
            else: self.view.failed_get("conference", id)
        except: print("Wrong data type!")

    def update_phone(self, phone, p_id):
        try:
            int(phone); p_id = int(p_id)
            if len(phone) == 10 and int(phone) >= 0:
                phon = self.model.get_items("phones", ("+38" + phone))
                if phon:
                    part = self.model.get_items("participants", p_id)
                    if part:
                        try:
                            self.model.update_phone(phon, part)
                            print("Phone was successfuly updated!")
                        except: print("A telephone with this number already exist!")
                    else: self.view.failed_get("participant", p_id)
                else: self.view.failed_get("phone", ("+38" + phone))
            else: print("Wrong telephone number length")
        except: print("Wrong data type!")

#---------------GENERATE FOR TABLE---------------
    def generate_data(self, table, num):
        try:
            int(num)
            if table == "audiences": self.model.generate_audiences(num)
            elif table == "participants": self.model.generate_participants(num)
            elif table == "conferences": self.model.generate_conferences(num)
            elif table == "phones": self.model.generate_phones(num)
            elif table == "links": self.model.generate_links(num)
            self.view.successful_generating(table)
        except: print("Wrong data type!")

#---------------FIND IN TABLE---------------
    def query1(self, c_id, age_min, count_phone_min):
        try:
            int(c_id); int(age_min); int(count_phone_min)
            conf = self.model.get_items("conferences", c_id)
            if conf:
                if int(age_min) >= 0 and int(count_phone_min) >= 0:
                    plist = self.model.query1(c_id, age_min, count_phone_min)
                    self.view.show_participants(plist)
                else: print("Negative values!")
            else: self.view.failed_get("conference", c_id)
        except: print("Wrong data type!")

    def query2(self, c_id):
        try:
            int(c_id)
            res = self.model.query2(c_id)
            if res: self.view.show_avg_age(res)
            else: self.view.failed_get("conference", c_id)
        except: print("Wrong data type!")

    def query3(self, corp, first_date, second_date):
        try:
            int(corp)
            res = self.model.query3(corp, first_date, second_date)
            self.view.show_topics(res)
        except: print("Wrong data type!")