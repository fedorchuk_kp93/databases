class View(object):

    @staticmethod
    def successful_deleting(itype, old_id):
        print("{} with {} id was successfuly deleted".format(itype, old_id))

    @staticmethod
    def successful_creating(itype, new_id):
        print ("Added {} has now {} id".format(itype, new_id))
    
    @staticmethod
    def successful_updating(itype):
        print ("A {} was successfuly updated".format(itype))
    
    @staticmethod
    def successful_generating(itype):
        print ("Data to {} was successfuly generated".format(itype))

    @staticmethod
    def failed_get(itype, id):
        print ("No {} with {} id".format(itype, id))

    @staticmethod
    def show_participants(participants):
        print("------Participants List------")
        for part in participants:
            print("{} {}".format(part[1], part[0]))

    @staticmethod
    def show_avg_age(age):
        print ("An average age of participants of this conference is {} years old".format(age[0][0]))

    @staticmethod
    def show_topics(topics):
        print("------Topics List------")
        for topic in topics:
            print("{}".format(topic[0]))