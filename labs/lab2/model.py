import psycopg2
from time import time

class Model(object):

    def __init__(self, dbname):
        self.database_name = dbname
        self.connection = None
        self.cursor = None

#---------------CONNEECTING METHODS---------------
    def connect(self):
        try:
            self.connection = psycopg2.connect(user = "postgres",password = "miahvbl0276",
            host = "127.0.0.1", port="5432",database=self.database_name)
            self.cursor = self.connection.cursor()
        except(Exception, psycopg2.Error) as error:
            print("Connection Error!", error)

    def close(self):
        if self.connection:
            self.cursor.close()
            self.connection.close()

    def reconnect(self):
        self.close()
        self.connect()

#---------------GENERAL METHODS---------------
    def get_items(self, table, column, value):
        try:
            self.cursor.execute("SELECT * FROM {} WHERE {} = '{}'".format(table, column, value))
            self.connection.commit()
            return self.cursor.fetchall()
        except:
            print ("Error: wrong value type!")
            self.reconnect()

    def get_link(self, table, column1, value1, column2, value2):
        try:
            self.cursor.execute("SELECT * FROM {} WHERE {} = '{}' AND {} = '{}'"
                                .format(table,column1,value1,column2,value2))
            self.connection.commit()
            return self.cursor.fetchall()
        except:
            print ("Error: wrong value type!")
            self.reconnect()

    def delete_items(self, table, column, value):
        try:
            self.cursor.execute("DELETE FROM {} WHERE {} = '{}'".format(table, column, value))
            self.connection.commit()
        except:
            print("Delete Error: wrong value type!")
            self.reconnect()

    def delete_link(self, table, column1, value1, column2, value2):
        try:
            self.cursor.execute("DELETE FROM {} WHERE {} = '{}' AND {} = '{}'"
                                .format(table,column1,value1,column2,value2))
            self.connection.commit()
        except:
            print("Delete Error: wrong value type!")
            self.reconnect()

#---------------CREATING METHODS---------------
    def create_participant(self, name, surname, age):
        self.cursor.execute("INSERT INTO participants (name, surname, age) VALUES ('{}', '{}', {}) RETURNING id".format(name, surname, age))
        self.connection.commit()
        return self.cursor.fetchall()

    def create_audience(self, num, corps_num):
        self.cursor.execute("INSERT INTO audiences (audience_number, corps_number) VALUES ({}, {}) RETURNING id".format(num, corps_num))
        self.connection.commit()
        return self.cursor.fetchall()

    def create_conference(self, date, topic, a_id):
        self.cursor.execute("INSERT INTO conferences (conference_date, conference_topic, audience_id)"
                            " VALUES ('{}', '{}', {}) RETURNING id".format(date, topic, a_id))
        self.connection.commit()
        return self.cursor.fetchall()

    def create_phone(self, num, p_id):
        try:
            self.cursor.execute("INSERT INTO phones (phone, participant_id) VALUES ('{}', {})".format(num, p_id))
            self.connection.commit()
        except: self.reconnect()

    def create_pc_link(self, c_id, p_id):
        try:
            self.cursor.execute("INSERT INTO conference_participant (conference_id, participant_id) VALUES ({}, {})".format(c_id, p_id))
            self.connection.commit()
        except: self.reconnect()

#---------------UPDATING METHODS---------------
    def update_participant(self, id, name, surname, age):
        self.cursor.execute("UPDATE participants SET name = '{}', surname = '{}', age = {}"
                            " WHERE id = {}".format(name, surname, age, id))
        self.connection.commit()

    def update_audience(self, id, num, corps_num):
        self.cursor.execute("UPDATE audiences SET audience_number = {}, corps_number = {}"
                            " WHERE id = {}".format(num, corps_num, id))
        self.connection.commit()

    def update_conference(self, id, date, topic, a_id):
        self.cursor.execute("UPDATE audiences SET conference_date = '{}', conference_topic = '{}',"
                            " audience_id = {} WHERE id = {}".format(date, topic, a_id, id))
        self.connection.commit()

    def update_phone(self, num, p_id):
        try:
            self.cursor.execute("UPDATE phones SET participant_id = {} WHERE phone = '{}'".format(p_id, num))
            self.connection.commit()
        except: self.reconnect()

#---------------GENERATING METHODS---------------
    def generate_audiences(self, num):
        self.cursor.execute("INSERT INTO audiences (audience_number, corps_number)"
	                        " SELECT num, corps FROM"
	                        " (SELECT trunc(1+random()*600)::int AS num, trunc(1+random()*20)::int"
                            " AS corps FROM generate_series(1,{}))"
                            " AS auds LEFT JOIN audiences ON num = audience_number AND corps = corps_number"
                            " WHERE audience_number IS null".format(num))
        self.connection.commit()

    def generate_participants(self, num):
        self.cursor.execute("INSERT INTO participants (name, surname, age)"
                            " SELECT chr(trunc(65+random()*25)::int) || random_str(3+(random()*5)::int),"
                            " chr(trunc(65+random()*25)::int) || random_str(4+(random()*6)::int),"
                            " (18+(random()*10)::int) FROM generate_series(1,{})".format(num))
        self.connection.commit()

    def generate_conferences(self, num):
        self.cursor.execute("INSERT INTO conferences (conference_date, conference_topic, audience_id)"
                            " SELECT random_date(), random_str(6 + (random()*10)::int), random_audience()"
                            " FROM generate_series(1, {})".format(num))
        self.connection.commit()


    def generate_phones(self, num):
        self.cursor.execute("INSERT INTO phones (phone, participant_id)"
                            " SELECT num, p_id FROM"
                            " (SELECT random_participant() AS p_id, num FROM"
                            " (SELECT random_phone_ua() AS num FROM generate_series(1,{}) GROUP BY num) AS phns) AS gen_phns"
                            " LEFT JOIN phones ON num = phone WHERE phone IS null".format(num))
        self.connection.commit()

    def generate_links(self, num):
        self.cursor.execute("INSERT INTO conference_participant (conference_id, participant_id)"
                            " WITH glinks AS (SELECT random_conference() AS cid, random_participant() AS pid"
                            " FROM generate_series(1, {}) GROUP BY cid, pid)"
                            " SELECT cid, pid FROM glinks LEFT JOIN conference_participant"
                            " ON glinks.cid = conference_id AND glinks.pid = participant_id"
                            " WHERE conference_id is null".format(num))
        self.connection.commit()

#---------------FINDING METHODS---------------
    def query1(self, c_id, age_min, count_phone_min):
        start_time = time()
        self.cursor.execute("with ids as( with cph as (select participant_id as p_id, count(participant_id)"
            " from participants inner join phones on participant_id = id"
            " group by participant_id)"
            " select p_id from cph where count >= {})"
            " select name, surname from("
            " select name, surname, id from conference_participant inner join participants"
            " on id = participant_id where conference_id = {} and age >= {}) as ps inner join"
            " ids on p_id = id".format(count_phone_min, c_id, age_min))
        self.connection.commit()
        execution_time = time() - start_time
        print("\nTime: {}\n".format(execution_time))
        return self.cursor.fetchall()

    def query2(self, c_id):
        start_time = time()
        self. cursor.execute("select sum(age)/count(conference_id) from participants inner join"
                            " conference_participant on participant_id = id where"
                            " conference_id = {} group by conference_id".format(c_id))
        self.connection.commit()
        execution_time = time() - start_time
        print("\nTime: {}\n".format(execution_time))
        return self.cursor.fetchall()

    def query3(self, corp, first_date, second_date):
        start_time = time()
        self. cursor.execute("select conference_topic from conferences inner join"
                            " (select id from audiences where corps_number = {}) as ids"
                            " on  ids.id = audience_id where conference_date > '{}'"
                            " and conference_date < '{}'".format(corp, first_date, second_date))
        self.connection.commit()
        execution_time = time() - start_time
        print("\nTime: {}\n".format(execution_time))
        return self.cursor.fetchall()
