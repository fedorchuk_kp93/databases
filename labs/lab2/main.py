from controller import Controller
from datetime import datetime
from menu import Menu
m = Menu()
c = Controller("ConferencesDB")
quit = False

while not(quit):
    m.separetor()
    m.start_menu()
    m.separetor()
    choice1 = input("Enter your choice: ")
    m.separetor()
    if choice1 == "1":
        m.show_items()
        m.separetor()
        choice2 = input("Enter your choice: ")
        if choice2 == "1":
            num = input("Enter audience number: ")
            corp = input("Enter corps number: ")
            m.separetor()
            c.add_audience(num, corp)
        elif choice2 == "2":
            name = input("Entrer name: ")
            sur = input("Enter surname: ")
            age = input("Enter age: ")
            m.separetor()
            c.add_participant(name, sur, age)
        elif choice2 == "3":
            topic = input("Enter topic: ")
            a_id = input("Enter audience id: ")
            year = input("Enter year: ")
            month = input("Enter month(int): ")
            day = input("Enter day: ")
            m.separetor()
            try:
                if int(year) >= 2010 and int(year) < 2030: 
                    date = datetime(int(year), int(month), int(day))
                    c.add_conference(date, topic, a_id)
                else: print("Bad date!")
            except: print("Bad date!")
        elif choice2 == "4":
            phone = input("Enter phone number(like 0XXXXXXXXX): ")
            p_id = input("Enter participant id: ")
            m.separetor()
            c.add_phone(phone, p_id)
        elif choice2 == "5":
            p_id = input("Enter participant id: ")
            c_id = input("Enter conference id: ")
            m.separetor()
            c.join_participant(c_id, p_id)
    elif choice1 == "2":
        m.show_items()
        m.separetor()
        choice2 = input("Enter your choice: ")
        if choice2 == "1":
            id = input("Enter audience id: ")
            m.separetor()
            au = c.get_item_by_id("audiences", id)
            if au:
                num = input("Old audience number: {}\tNew: ".format(au[0][1]))
                corp = input("Old corps number: {}\tNew: ".format(au[0][2]))
                m.separetor()
                c.update_audience(id, num, corp)
        elif choice2 == "2":
            id = input("Enter participant id: ")
            m.separetor()
            part = c.get_item_by_id("participants", id)
            if part:
                name = input("Old participant name: {}\tNew: ".format(part[0][1]))
                surname = input("Old participant surname: {}\tNew: ".format(part[0][2]))
                age = input("Old participant age: {}\tNew: ".format(part[0][3]))
                m.separetor()
                c.update_participant(id, name, surname, age)
        elif choice2 == "3":
            id = input("Enter conference id: ")
            m.separetor()
            conf = c.get_item_by_id("conferences", id)
            if conf:
                topic = input("Old conference topic: {}\nNew topic: ".format(conf[0][2]))
                a_id = input("Old audience id: {}\nNew audience id: ".format(conf[0][3]))
                year = input("Old conference date: {}\nNew year: ".format(conf[0][1]))
                month = input("New month(int): ")
                day = input("New day: ")
                m.separetor()
                try:
                    if int(year) >= 2010 and int(year) < 2030: 
                        date = datetime(int(year), int(month), int(day))
                        c.update_conference(id, date, topic, a_id)
                    else: print("Bad date!")
                except: print("Bad date!")
        elif choice2 == "4":
            phone_num = input("Enter phone number(like 0XXXXXXXXX): ")
            m.separetor()
            phone = c.get_phone(phone_num)
            if phone:
                p_id =  input("Old owner id: {}\nNew owner id: ".format(conf[0][1]))
                m.separetor()
                c.update_phone(("+38" + phone), p_id)
        elif choice2 == "5":
            try:
                ocid = input("Enter conference id: ")
                opid = input("Enter participant id: ")
                m.separetor()
                c.leave_conference(ocid, opid)
                ncid = input("Enter new conference id: ")
                npid = input("Enter new participant id: ")
                m.separetor()
                c.join_participant(ncid, npid)
            except: pass
    elif choice1 == "3":
        m.show_items()
        m.separetor()
        choice2 = input("Enter your choice: ")
        if choice2 == "1": 
            id = input("Enter audience id: ")
            m.separetor()
            c.delete_audience(id)
        elif choice2 == "2": 
            id = input("Enter participant id: ")
            m.separetor()
            c.delete_participant(id)
        elif choice2 == "3": 
            id = input("Enter conference id: ")
            m.separetor()
            c.delete_conference(id)
        elif choice2 == "4":
            phone_num = input("Enter phone number(like 0XXXXXXXXX): ")
            m.separetor()
            c.delete_phone(phone_num)
        elif choice2 == "5":
            cid = input("Enter conference id: ")
            pid = input("Enter participant id: ")
            m.separetor()
            c.leave_conference(cid, pid)
    elif choice1 == "4":
        m.show_items()
        m.separetor()
        choice2 = input("Enter your choice: ")
        if choice2 == "1":
            num = input("Enter count of data: ")
            m.separetor()
            c.generate_data("audiences", num)
        elif choice2 == "2":
            num = input("Enter count of data: ")
            m.separetor()
            c.generate_data("participants", num)
        elif choice2 == "3":
            num = input("Enter count of data: ")
            m.separetor()
            c.generate_data("conferences", num)
        elif choice2 == "4":
            num = input("Enter count of data: ")
            m.separetor()
            c.generate_data("phones", num)
        elif choice2 == "5":
            num = input("Enter count of data: ")
            m.separetor()
            c.generate_data("links", num)
    elif choice1 == "5":
        print("This query shows you all particiants of confrence\nwhich are older than minimal age")
        print("and which have more than minimal count of telephones\n")
        c_id = input("Enter conference id: ")
        min_age = input("Enter mimimal age: ")
        count_phones = input("Enter minimal count of phones: ")
        m.separetor()
        c.query1(c_id, min_age, count_phones)
    elif choice1 == "6":
        print("This query shows you the average age of participants\nwhich participate in particular conference\n")
        c_id = input("Enter conference id: ")
        c.query2(c_id)
    elif choice1 == "7":
        print("This query shows all of conference's topic\nwhich are planned to be held in a particular corpus\n"
                "and which are planed later the first date\nand earlier than the second date\n")
        corp = input("Enter corps number: ")
        year1 = input("First date year: ")
        month1 = input("First date month(int): ")
        day1 = input("First date day: ")
        m.separetor()
        try:
            date1 = datetime(int(year1), int(month1), int(day1))
            year2 = input("Second date year: ")
            month2 = input("Second date month(int): ")
            day2 = input("Second date day: ")
            m.separetor()
            try:
                date2 = datetime(int(year2), int(month2), int(day2))
                c.query3(corp, date1, date2)
            except: print("Bad date!")
        except: print("Bad date!")
    elif choice1 == "8": quit = True; print("Exiting...")
    else: print("Plese, enter something from list!")


# name = input("Enter name: ")
# surname = input("Enter surname: ")
# c.add_audience(name, surname)
