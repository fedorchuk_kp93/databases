from model import Model
from view import View

class Controller(object):

    def __init__(self, dbname):
        self.view = View()
        self.model = Model(dbname)
        self.model.connect()

    def __del__(self):
        self.model.close()

#---------------GET FROM TABLE---------------
    def get_item_by_id(self, table, id):
        res = self.model.get_items(table, "id", id)
        if res: return res
        else: self.view.failed_get(table, id)

    def get_phone(self, phone):
        try:
            int(phone)
            if len(phone) == 10:
                res = self.model.get_items("phones", "phone", ("+38" + phone)) 
                if res: return res
                else: self.view.failed_get("phone", ("+38" + phone))
            else: print("Wrong telephone number length")
        except: print("Wrong data type!")

#---------------DELETE FROM TABLE---------------
    def delete_audience(self, a_id):
        aud = self.model.get_items("audiences", "id", a_id)
        if aud:
            lconfs = self.model.get_items("conferences", "audience_id", a_id)
            for conf in lconfs:
                self.delete_conference(conf[0])
            self.model.delete_items("audiences", "id", a_id)
            self.view.successful_deleting("Audience", a_id)
        else: self.view.failed_get("audience", a_id)

    def delete_participant(self, p_id):
        participant = self.model.get_items("participants", "id", p_id)
        if participant:
            self.model.delete_items("phones", "participant_id", p_id)
            self.model.delete_items("conference_participant", "participant_id", p_id)
            self.model.delete_items("participants", "id", p_id)
            self.view.successful_deleting("Participant", p_id)
        else: self.view.failed_get("participant", p_id)

    def delete_conference(self, c_id):
        conf = self.model.get_items("conferences", "id", c_id)
        if conf:
            self.model.delete_items("conference_participant", "conference_id", c_id)
            self.model.delete_items("conferences", "id", c_id)
            self.view.successful_deleting("Conference", c_id)
        else: self.view.failed_get("conference", c_id)

    def delete_phone(self, phone):
        try:
            int(phone)
            if len(phone) == 10:
                phon = self.model.get_items("phones", "phone", ("+38" + phone))
                if phon:
                    self.model.delete_items("phones", "phone", ("+38" + phone)) 
                    print("Phone {} was successfuly deleted!".format(("+38" + phone)))
                else: self.view.failed_get("phone", ("+38" + phone))
            else: print("Wrong telephone number length")
        except: print("Wrong data type!")

    def leave_conference(self, c_id, p_id):
        link = self.model.get_link("conference_participant", "conference_id", c_id, "paricipant_id", p_id)
        if link:
            self.model.delete_link("conference_participant", "conference_id", c_id, "paricipant_id", p_id)
            print ("Link was successfuly deleted!")
        print("This link doesn't exist")

#---------------ADD TO TABLE---------------
    def add_participant(self, name, surname, age):
        try:
            age = int(age)
            if len(name) and len(surname):
                if age >= 18 and age < 90:
                    newid = self.model.create_participant(name, surname, age)[0][0]
                    self.view.successful_creating("participant", newid)
                    return newid
                else:
                    print("Age out of range!")
            else:
                print("Empty values!")
        except: print("Wrong data type!")

    def add_audience(self, num, corps_num):
        try:
            num = int(num); corps_num = int(corps_num)
            if num > 0 and corps_num > 0:
                newid = self.model.create_audience(num, corps_num)[0][0]
                self.view.successful_creating("audience", newid)
                return newid
            else:
                print("Bad values!")
        except: print("Wrong data type!")

    def add_conference(self, date, topic, a_id):
        au = self.model.get_items("audiences", "id", a_id)
        if au:
            if len(topic) > 0:
                newid = self.model.create_conference(date, topic, a_id)[0][0]
                self.view.successful_creating("conference", newid)
                return newid
            else:
                print("Empty topic!")
        else: print("No audience with {} id!".format(a_id))

    def add_phone(self, phone, p_id):
        try:
            int(phone); p_id = int(p_id)
            if len(phone) == 10:
                part = self.model.get_items("participants", "id", p_id)
                if part:
                    try:
                        self.model.create_phone(("+38" + phone), p_id)
                        print("New phone was successfuly added!")
                    except: print("A telephone with this number already exist!")
                else: self.view.failed_get("participant", p_id)
            else: print("Wrong telephone number length")
        except: print("Wrong data type!")

    def join_participant(self, c_id, p_id):
        participant = self.model.get_items("participants", "id", p_id)
        conf = self.model.get_items("conferences", "id", c_id)
        if conf and participant:
            try:
                self.model.create_pc_link(c_id, p_id)
            except: print("Error this link just exist")

#---------------UPDATE IN TABLE---------------
    def update_audience(self, id, num, corp):
        try:
            int(num), int(corp)
            if num > 0 and corp > 0:
                self.model.update_audience(id, num, corp)
                self.view.successful_updating("audience")
            else: print("Bad values!")
        except: print("Wrong data type!")

    def update_participant(self, id, name, surname, age):
        try:
            age = int(age)
            if len(name) and len(surname):
                if age >= 18 and age < 90:
                    self.model.update_participant(id, name, surname, age)
                    self.view.successful_updating("participant")
                else:
                    print("Age out of range!")
            else:
                print("Empty values!")
        except: print("Wrong data type!")
    
    def update_conference(self, id, date, topic, a_id):
        try:
            au = self.model.get_items("audiences", "id", a_id)
            if au:
                if len(topic) > 0:
                    self.model.update_conference(id, date, topic, a_id)
                    self.view.successful_updating("conference")
                else:
                    print("Empty topic!")
            else: self.view.failed_get("audience", a_id)
        except: print("Wrong data type!")

    def update_phone(self, phone, p_id):
        try:
            int(phone); p_id = int(p_id)
            if len(phone) == 10 and int(phone) >= 0:
                part = self.model.get_items("participants", "id", p_id)
                if part:
                    try:
                        self.model.update_phone(("+38" + phone), p_id)
                        print("Phone was successfuly updated!")
                    except: print("A telephone with this number already exist!")
                else: self.view.failed_get("participant", p_id)
            else: print("Wrong telephone number length")
        except: print("Wrong data type!")

#---------------GENERATE FOR TABLE---------------
    def generate_data(self, table, num):
        try:
            int(num)
            if table == "audiences": self.model.generate_audiences(num)
            elif table == "participants": self.model.generate_participants(num)
            elif table == "conferences": self.model.generate_conferences(num)
            elif table == "phones": self.model.generate_phones(num)
            elif table == "links": self.model.generate_links(num)
            self.view.successful_generating(table)
        except: print("Wrong data type!")

#---------------FIND IN TABLE---------------
    def query1(self, c_id, age_min, count_phone_min):
        try:
            int(c_id); int(age_min); int(count_phone_min)
            conf = self.model.get_items("conferences", "id", c_id)
            if conf:
                if int(age_min) >= 0 and int(count_phone_min) >= 0:
                    plist = self.model.query1(c_id, age_min, count_phone_min)
                    self.view.show_participants(plist)
                else: print("Negative values!")
            else: self.view.failed_get("conference", c_id)
        except: print("Wrong data type!")

    def query2(self, c_id):
        try:
            int(c_id)
            res = self.model.query2(c_id)
            if res: self.view.show_avg_age(res)
            else: self.view.failed_get("conference", c_id)
        except: print("Wrong data type!")

    def query3(self, corp, first_date, second_date):
        try:
            int(corp)
            res = self.model.query3(corp, first_date, second_date)
            self.view.show_topics(res)
        except: print("Wrong data type!")