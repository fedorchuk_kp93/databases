class Menu(object):

    @staticmethod
    def separetor():
        print("-----------------------------------------------------------------------")
    
    @staticmethod
    def start_menu():
        print("1. Create")
        print("2. Update")
        print("3. Delete")
        print("4. Generate")
        print("5. Query1")
        print("6. Query2")
        print("7. Query3")
        print("8. Exit")

    @staticmethod
    def show_items():
        print("1. Audience")
        print("2. Participant")
        print("3. Conference")
        print("4. Phone")
        print("5. Link")
        print("Else: go back")