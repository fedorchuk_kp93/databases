from sqlalchemy import Column, String, Integer, Date, ForeignKey, CHAR, Float
from sqlalchemy.orm import relationship, backref
from base import Base

class Region(Base):
    __tablename__ = 'regions'

    id = Column(Integer, primary_key=True)
    regname = Column(String)
    areas = relationship("Area", back_populates='region',
                        cascade="all, delete, delete-orphan")

    def __init__(self, regname):
        self.regname = regname

class Area(Base):
    __tablename__ = 'areas'

    id = Column(Integer, primary_key=True)
    areaname = Column(String)
    reg_id = Column(Integer, ForeignKey('regions.id'))
    region = relationship("Region", back_populates='areas')
    teritories = relationship("Teritory", back_populates='area',
                        cascade="all, delete, delete-orphan")

    def __init__(self, areaname, region):
        self.areaname = areaname
        self.reg_id = region

class Teritory(Base):
    __tablename__ = 'teritories'

    id = Column(Integer, primary_key=True)
    tername = Column(String)
    area_id = Column(Integer, ForeignKey('areas.id'))
    area = relationship("Area", back_populates='teritories')
    participants = relationship("Participant", back_populates='teritory',
                        cascade="all, delete, delete-orphan")

    def __init__(self, tername, area):
        self.tername = tername
        self.area_id = area

class Status(Base):
    __tablename__ = 'participant_status'

    id = Column(Integer, primary_key=True)
    status = Column(String)
    participants = relationship("Participant", back_populates='status',
                        cascade="all, delete, delete-orphan")

    def __init__(self, status):
        self.status = status

class Participant(Base):
    __tablename__ = 'participants'

    outid = Column(String, primary_key=True)
    sextype = Column(String)
    birth = Column(Integer)
    ter_id = Column(Integer, ForeignKey('teritories.id'))
    status_id = Column(Integer, ForeignKey('participant_status.id'))
    teritory = relationship("Teritory", back_populates='participants')
    status = relationship("Status", back_populates='participants')
    scores = relationship("Score", back_populates='participant', cascade="all, delete, delete-orphan")

    def __init__(self, outid, sextype, birth, teritory, status):
        self.outid = outid
        self.sextype = sextype
        self.birth = birth
        self.ter_id = teritory
        self.status_id = status

class Subject(Base):
    __tablename__ = 'subjects'

    id = Column(Integer, primary_key=True)
    subject = Column(String)
    scores = relationship("Score", back_populates='subject',
                        cascade="all, delete, delete-orphan")

    def __init__(self, subject):
        self.subject = subject

class Score(Base):
    __tablename__ = 'scores'

    participant_id = Column(String, ForeignKey('participants.outid'), primary_key=True)
    subject_id = Column(Integer, ForeignKey('subjects.id'), primary_key=True)
    participant = relationship("Participant", back_populates='scores')
    subject = relationship("Subject", back_populates='scores')
    year = Column(Integer)
    score = Column(Float)

    def __init__(self, participant, subject, year, score):
        self.participant = participant
        self.subject_id = subject
        self.year = year
        self.score = score