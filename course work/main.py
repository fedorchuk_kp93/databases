from controller import Controller
from menu import Menu
import csv
import os

menu = Menu()
c = Controller("ZNO_Statistic")
subjects_choices = [
    { "subject": "UkrLanguage", "chosen": False },
    { "subject": "History", "chosen": False },
    { "subject": "Math", "chosen": False },
    { "subject": "Physics", "chosen": False },
    { "subject": "Chemistry", "chosen": False },
    { "subject": "Biology", "chosen": False },
    { "subject": "Geography", "chosen": False },
    { "subject": "EngLanguage", "chosen": False }
]
tertypes = { "2": "регіону", "3": "району"}

quit = False
while (not quit):
    menu.separetor()
    menu.main_menu()
    choice1 = menu.get_choice()
    menu.separetor()
    if (choice1 == "1"):
        menu.display_teritories()
        choice2 = menu.get_choice()
        if choice2 not in ["1", "2", "3"]: continue
        menu.separetor()
        regions = c.display_and_return_regions()
        if choice2 == "1": menu.wait(); continue

        regnum = input("\nВведіть номер потрібного регіону або введіть щось інше, щоб повернутися в головне меню: ")
        menu.separetor()
        areas = c.display_and_return_areas(regnum, regions)
        if areas == None: continue
        if choice2 == "2": menu.wait(); continue

        areanum = input("\nВведіть номер потрібного району або введіть щось інше, щоб повернутися в головне меню: ")
        menu.separetor()
        teritories = c.display_and_return_teritories(areas, areanum)
        if teritories == None: continue
        else: menu.wait(); continue

    elif (choice1 in ["2", "3"]):
        menu.display_teritories()
        choice2 = menu.before_ploting()
        if choice2 not in ["1", "2", "3"]: continue
        menu.separetor()
        name = ""
        if choice2 in ["2", "3"]: name = menu.get_ter_name(tertypes[choice2]); menu.separetor()
        chosen_subjects = subjects_choices
        firstIteration = True
        back = False
        subjects_choice = ""
        while (subjects_choice != "9" and not back):
            menu.display_subjects(firstIteration)
            subjects_choice = menu.get_choice()
            menu.separetor()
            if subjects_choice == "9" and not firstIteration: continue
            if subjects_choice not in ["1", "2", "3", "4", "5", "6", "7", "8"]: back = True; continue
            chosen_subjects[int(subjects_choice)-1]["chosen"] = True
            firstIteration = False
        if back: continue
        subjects = []
        for sub in chosen_subjects:
            if sub["chosen"]: subjects.append(sub["subject"])
        if choice1 == "2":
            ignore = False
            if menu.get_ignore() == 'так': ignore = True
            if (choice2 == "1"): c.analyse_Ukraine_avg(subjects, ignore)
            elif (choice2 == "2"): c.analyse_region_avg(name, subjects, ignore)
            elif (choice2 == "3"): c.analyse_area_avg(name, subjects, ignore)
        if choice1 == "3":
            if (choice2 == "1"): c.analyse_Ukraine_failure_percentage(subjects)
            elif (choice2 == "2"): c.analyse_region_failure_percentage(name, subjects)
            elif (choice2 == "3"): c.analyse_area_failure_percentage(name, subjects)
        for sub in chosen_subjects:
            sub["chosen"] = False

    elif (choice1 == "4"):
        c.backup()
    elif (choice1 == "5"):
        c.restore()
    elif (choice1 == "6"):
        year = input("Рік: ")
        delimiter = input("CSV розділювач: ")
        c.load_data_from_csv_file(year, delimiter)
    elif (choice1 == "7"):
        quit = True
    else:
        print("Будь ласка, введіть щось інше!")

print("Завершення прорами...")
