import matplotlib.pyplot as plt

colors = {
    "UkrLanguage":'blue',
    "History":'red',
    "Math":'cyan',
    "Physics":'black',
    "Chemistry":'yellow',
    "Biology":'green',
    "Geography":'magenta',
    "EngLanguage":'orange'
}

count_text = 'Кількість учасників'

_subjects = {
    "UkrLanguage":'Українська мова',
    "History":'Історія України',
    "Math":'Математика',
    "Physics":'Фізика',
    "Chemistry":'Хімія',
    "Biology":'Біологія',
    "Geography":'Географія',
    "EngLanguage":'Англійська мова'
}

class View(object):

    @staticmethod
    def show_avg_score_diagramm(lists, subjects, task, title):
        plt.subplot(1, 2, 1)
        i = 0
        for subject in subjects:
            plt.plot(lists[0], lists[1][i], marker='.', markersize=10, color=colors[subject], label=_subjects[subject])
            i += 1
        plt.title(f'{title}')
        plt.ylabel(task)
        plt.xticks(fontsize=6, rotation=50)
        plt.legend(loc='lower right', bbox_to_anchor=(1, 1))

        plt.subplot(1, 2, 2)
        i = 0
        for subject in subjects:
            plt.plot(lists[0], lists[2][i], marker='.', markersize=10, color=colors[subject], label=_subjects[subject])
            i += 1
        plt.title(f'{title}')
        plt.ylabel(count_text)
        plt.xticks(fontsize=6, rotation=50)
        plt.show()

    @staticmethod
    def show_regions(regions):
        i = 1
        for region in regions:
            print(f'{i}. {region.regname}')
            i += 1

    @staticmethod
    def show_areas(areas):
        i = 1
        for area in areas:
            print(f'{i}. {area.areaname}')
            i += 1
    
    @staticmethod
    def show_teritories(teritories):
        i = 1
        for teritory in teritories:
            print(f'{i}. {teritory.tername}')
            i += 1
        