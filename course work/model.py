import psycopg2
from time import time
from sqlalchemy import and_
from base import main_Session, main_engine, replica_Session, replica_engine, Base
from entities import Area, Subject, Region, Teritory, Status, Participant, Score

def convertStrToType(type_name):
    if type_name == "subjects": return Subject
    elif type_name == "regions": return Region
    elif type_name == "areas": return Area
    elif type_name == "teritories": return Teritory
    elif type_name == "participant_status": return Status
    elif type_name == "participants": return Participant
    elif type_name == "scores": return Score
    else: return None

class Model(object):

    def __init__(self, dbname):
        self.database_name = dbname
        self.connection = None
        self.cursor = None
        self.session1 = None
        self.session2 = None

    def __del__(self):
        self.close()

#---------------CONNEECTING METHODS---------------
    def connect(self):
        try:
            self.session1 = main_Session()
            self.session2 = replica_Session()
            
        except(Exception, psycopg2.Error) as error:
            print("Connection Error!", error)

    def close(self):
        if self.session1: self.session1.close()
        if self.session2: self.session2.close()

    def reconnect(self):
        self.close()
        self.connect()

    def get_all(self, table):
        try: return self.session2.query(convertStrToType(table)).all()
        except:
            print ("Error: wrong value type!")
            self.reconnect()

    def get_ter_scores(self, tername, subject):
        try:
            subject_id = self.get_subject_by_name(subject).id
            return self.session2.query(Score.score).filter(and_(Score.subject_id == subject_id,
                Score.participant_id.in_(self.session2.query(Participant.outid).filter(Participant.ter_id ==
                self.session2.query(Teritory.id).filter(Teritory.tername == tername).first()))))
        except:
            print ("Error: wrong query type!")
            self.reconnect()

    def get_area_scores(self, areaname, subject):
        try:
            subject_id = self.get_subject_by_name(subject).id
            return self.session2.query(Score.score).filter(and_(Score.subject_id == subject_id,
                Score.participant_id.in_(self.session2.query(Participant.outid).filter(Participant.ter_id.in_
                (self.session2.query(Teritory.id).filter(Teritory.area_id ==
                self.session2.query(Area.id).filter(Area.areaname == areaname).first()))))))
        except:
            print ("Error: wrong value type!")
            self.reconnect()

    def get_region_scores(self, regname, subject):
        try:
            subject_id = self.get_subject_by_name(subject).id
            return self.session2.query(Score.score).filter(and_(Score.subject_id == subject_id,
                Score.participant_id.in_(self.session2.query(Participant.outid).filter(Participant.ter_id.in_
                (self.session2.query(Teritory.id).filter(Teritory.area_id.in_
                (self.session2.query(Area.id).filter(Area.reg_id == 
                self.session2.query(Region.id).filter(Region.regname == regname).first()))))))))
        except:
            print ("Error: wrong value type!")
            self.reconnect()

    def get_item_by_id(self, table, value):
        try: return self.session2.query(convertStrToType(table)).get(value)
        except:
            print ("Error: wrong value type!")
            self.reconnect()
            return None

    def get_subject_by_name(self, name):
        try: return self.session2.query(Subject).filter_by(subject=name).first()
        except:
            print ("Error: wrong value type!")
            self.reconnect()
            return None

    def get_region_by_name(self, name):
        try: return self.session2.query(Region).filter_by(regname=name).first()
        except:
            print ("Error: wrong value type!")
            self.reconnect()
            return None

    def get_area_by_name(self, name):
        try: return self.session2.query(Area).filter_by(areaname=name).first()
        except:
            print ("Error: wrong value type!")
            self.reconnect()
            return None

    def get_ter_by_name(self, name):
        try: return self.session2.query(Teritory).filter_by(tername=name).first()
        except:
            print ("Error: wrong value type!")
            self.reconnect()
            return None

    def get_status(self, status):
        try: return self.session2.query(Status).filter_by(status=status).first()
        except:
            print ("Error: wrong value type!")
            self.reconnect()
            return None

    def get_score(self, participant_id, subject_id):
        try: return self.session2.query(Score).get((participant_id, subject_id))
        except:
            print ("Error: wrong value type!")
            self.reconnect()
            return None

    def create_reg(self, regname):
        try:
            new_region = Region(regname)
            self.session1.add(new_region)
            self.session1.commit()
        except: self.reconnect()

    def create_area(self, areaname, region):
        try:
            new_area = Area(areaname, region)
            self.session1.add(new_area)
            self.session1.commit()
        except: self.reconnect()

    def create_ter(self, tername, area):
        try:
            new_ter = Teritory(tername, area)
            self.session1.add(new_ter)
            self.session1.commit()
        except: self.reconnect()

    def create_pstatus(self, pstatus):
            new_status = Status(pstatus)
            self.session1.add(new_status)
            self.session1.commit()

    def create_participant(self, outid, sex, birth, teritory, status):
        try:
            new_participant = Participant(outid, sex, birth, teritory, status)
            self.session1.add(new_participant)
            self.session1.commit()
        except: self.reconnect()
    
    def create_score(self, participant, subject, year, score):
        try:
            new_score = Score(participant, subject, year, score)
            self.session1.add(new_score)
            self.session1.commit()
        except: self.reconnect()