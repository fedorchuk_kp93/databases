class Menu(object):

    @staticmethod
    def main_menu():
        print("1. Переглянути...")
        print("2. Побудувати графік середніх оцінок")
        print("3. Побудувати графік учасників, що не пройшли тестування")
        print("4. Зарезервувати базу даних")
        print("5. Відновити базу даних")
        print("6. Згенерувати базу даних з .csv файлу")
        print("7. Вийти")

    @staticmethod
    def display_subjects(first = True):
        print("1. Українська мова")
        print("2. Історія України")
        print("3. Математика")
        print("4. Фізика")
        print("5. Хімія")
        print("6. Біологія")
        print("7. Географія")
        print("8. Англійська мова")
        if not first:
            print("9. Побудувати")
        print("Інше. Повернутися в головне меню")

    @staticmethod
    def display_teritories():
        print("1. Регіони")
        print("2. Райони в регіоні")
        print("3. Території в районі")
        print("Інше. Повернутися в головне меню")

    @staticmethod
    def before_ploting():
        return input("Введіть тип територіальних одиниць, для яких буде здійснюватися аналіз: ")

    @staticmethod
    def get_choice():
        return input("\nВведіть ваш вибір: ")

    @staticmethod
    def get_ignore():
        return input("\nВведіть 'так', якщо хочете ігнорувати нульові оцінки: ")

    @staticmethod
    def get_ter_name(tertype):
        return input(f"\nВведіть назву {tertype}: ")

    @staticmethod
    def separetor():
        print("-----------------------------------------------------------------------")

    @staticmethod
    def wait():
        input("Введіть щось, щоб повернутися в головне меню: ")