import os
from _datetime import datetime,timedelta,date
import pandas as pd
import glob
import numpy as np
import csv
from model import Model
from view import View

def convertSexType(sextype):
    if sextype == "чоловіча": return "M"
    elif sextype == "жіноча": return "F"
    else: return None

fail_text = 'Відсоток учасників, що не подолали поріг або не з\'явилися на тестування(%)'
count_text = 'Кількість учасників'

subjects_prefixes = [
    {"prefix":'Ukr', "name":'UkrLanguage'},
    {"prefix":'hist', "name":'History'},
    {"prefix":'math', "name":'Math'},
    {"prefix":'phys', "name":'Physics'},
    {"prefix":'chem', "name":'Chemistry'},
    {"prefix":'bio', "name":'Biology'},
    {"prefix":'geo', "name":'Geography'},
    {"prefix":'eng', "name":'EngLanguage'},
    ]

class Controller(object):

    def __init__(self, dbname):
        self.model = Model(dbname)
        self.view = View()
        self.model.connect()

    def __del__(self):
        self.model.close()

    def restore(self):
        os.chdir(r'C:\\Users\\IVAN\\Documents\\studing\\DB corse work\\data\\backups\\')
        file_list = glob.glob('*.dump')
        file_list.append('Повернутися')
        files=pd.DataFrame(file_list,columns=['filename'])
        while 1:
            try:
                print(files)
                index_file = int(input('Enter file index: ').strip())
                if(index_file == len(file_list)-1): return
                if(index_file >= 0 and index_file < len(file_list)):
                    self.model.close()
                    os.system(f"""C:\\"Program Files"\\PostgreSQL\\12\\bin\\dropdb.exe -h 127.0.0.1 -U postgres  -i -e ZNO_Statistic """)
                    os.system(f"""C:\\"Program Files"\\PostgreSQL\\12\\bin\\createdb.exe -h 127.0.0.1 -U postgres  -E UTF-8 -e ZNO_Statistic """)
                    if(os.system(f"""C:\\"Program Files"\\PostgreSQL\\12\\bin\\psql.exe -h 127.0.0.1 -U postgres ZNO_Statistic < C:\\Users\\IVAN\\Documents\\studing\\"DB corse work"\\data\\backups\\{files.loc[index_file,'filename']}""")==0):
                        print("Successful restore!")
                    else:print("You have problem with restore!")
                    self.model.connect()
                    return
                else:print('Введіть інше значення')
            except:print('Wrong value type!')

    def backup(self):
        today = datetime.today()
        fileName = 'ZNO_Statistic_reserve'
        completeName = "{}_{}".format(fileName, today.strftime('%Y%m%d_%H%M%S'))
        backup_str = f"""C:\\"Program Files"\\PostgreSQL\\12\\bin\\pg_dump.exe -h 127.0.0.1 -U postgres ZNO_Statistic > C:\\Users\\IVAN\\Documents\\studing\\"DB corse work"\\data\\backups\\{completeName}.dump"""
        if(os.system(backup_str)==0):
            print("Успішно зарезервовано!")
        else:print("Error: failure backup!")
        return

    def insert_reg(self, regname):
        if not self.model.get_region_by_name(regname):
            self.model.create_reg(regname)

    def insert_area(self, areaname, regname):
        if not self.model.get_area_by_name(areaname):
            region = self.model.get_region_by_name(regname)
            if region:
                self.model.create_area(areaname, region.id)
            else: print(f'No region with {regname} name!')

    def insert_ter(self, tername, areaname):
        if not self.model.get_ter_by_name(tername):
            area = self.model.get_area_by_name(areaname)
            if area:
                self.model.create_ter(tername, area.id)
            else: print(f'No area with {areaname} name!')

    def insert_pstatus(self, pstatus):
        if not self.model.get_status(pstatus):
            self.model.create_pstatus(pstatus)

    def insert_participant(self, outid, sex, birth, ter, pstatus):
        if not self.model.get_item_by_id("participants", outid):
            try:
                int(birth)
                teritory = self.model.get_ter_by_name(ter)
                if teritory:
                    sextype = convertSexType(sex)
                    if sextype:
                        status = self.model.get_status(pstatus)
                        if status:
                            self.model.create_participant(outid, sextype, birth, teritory.id, status.id)
                        else: print(f'No status with {pstatus} name!')
                    else: print(f'No sex type {sex}!')
                else: print(f'No teritory with {ter} name!')
            except: print("Wrong value type!")

    def add_scores(self, line, year):
        try:
            if (int(year) <= 2020 and int(year) >= 2010):
                part = self.model.get_item_by_id("participants", line["OUTID"])
                if part:
                    for subject in subjects_prefixes:
                        if (not line["{}TestStatus".format(subject["prefix"])] == "null"):
                            _subject = self.model.get_subject_by_name(subject["name"])
                            link = self.model.get_score(line["OUTID"], _subject.id)
                            if not link:
                                if (line["{}Ball100".format(subject["prefix"])] == "null"): score = 0
                                else: score = line["{}Ball100".format(subject["prefix"])].replace(",", ".")
                                self.model.create_score(part, _subject.id, year, score)
                else: print(f'No participant with {line["OUTID"]} id!')
            else: print("Wrong year!")
        except: print("Wrong value type!")

    def load_data_from_csv_file(self, year, delimiter):
        if (int(year) <= 2020 and int(year) >= 2010):
            try:
                os.chdir(r'C:\\Users\\IVAN\\Documents\\studing\\DB corse work\\data\\')
                file_list = glob.glob('*.csv')
                file_list.append('Повернутися')
                files = pd.DataFrame(file_list,columns=['filename'])
                while 1:
                    try:
                        print(files)
                        index_file = int(input('Enter file index: ').strip())
                        if(index_file == len(file_list)-1): return
                        if(index_file >= 0 and index_file < len(file_list)):
                            csv_path = f"""C:\\Users\\IVAN\\Documents\\studing\\DB corse work\\data\\{files.loc[index_file,'filename']}"""
                            print(csv_path)
                            try:
                                with open(csv_path, "r") as f_obj:
                                    reader = csv.DictReader(f_obj, delimiter=delimiter)
                                    for line in reader:
                                        self.insert_reg(line["REGNAME"])
                                        self.insert_area(line["AREANAME"], line["REGNAME"])
                                        self.insert_ter(line["TERNAME"], line["AREANAME"])
                                        self.insert_pstatus(line["REGTYPENAME"])
                                        self.insert_participant(line["OUTID"], line["SEXTYPENAME"], line["Birth"], line["TERNAME"], line["REGTYPENAME"])
                                        self.add_scores(line, year)
                                    return
                            except: print("Bad file path or wrong delimiter")
                        else:print('This value is not included in the possible values, try again')
                    except:print('Wrong value type!')
            except: print("Bad file path or wrong delimiter")
        else: print("Wrong year!")

    def display_and_return_regions(self):
        regions = self.model.get_all("regions")
        self.view.show_regions(regions)
        return regions

    def display_and_return_areas(self, regnum, regions):
        try: regnum = int(regnum)
        except: return None
        i = 1
        for region in regions:
            if regnum == i:
                self.view.show_areas(region.areas)
                return region.areas
            i += 1
        return None

    def display_and_return_teritories(self, areanum, areas):
        try: areanum = int(areanum)
        except: return None
        i = 1
        for area in areas:
            if areanum == i:
                self.view.show_teritories(area.teritories)
                return area.teritories
            i += 1
        return None
    
    def analyse_Ukraine_avg(self, subjects, ignore0points = True):
        regions = self.model.get_all("regions")
        scores_lists = []
        counts_list = []
        regions_list = []
        first = True
        for subject in subjects:
            scores_list = []
            counts = []
            for region in regions:
                if first: regions_list.append(region.regname)
                scores = self.model.get_region_scores(region.regname, subject)
                print(scores)
                _scores = np.array(scores.all(), dtype="float32")
                _scores.ravel()
                if ignore0points:
                    _scores = _scores[_scores > 0]
                else: _scores[_scores == 0] = 100
                counts.append(_scores.size)
                if _scores.size == 0: scores_list.append(0)
                else: scores_list.append(round(np.average(_scores), 1))
            first = False
            scores_lists.append(scores_list)
            counts_list.append(counts)
        lists = [regions_list, scores_lists, counts_list]
        self.view.show_avg_score_diagramm(lists, subjects, 'Середній бал (200)', 'Україна')

    def analyse_region_avg(self, regname, subjects, ignore0points = True):
        region = self.model.get_region_by_name(regname)
        if region:
            areas = region.areas
            scores_lists = []
            counts_list = []
            areas_list = []
            first = True
            for subject in subjects:
                scores_list = []
                counts = []
                for area in areas:
                    if first: areas_list.append(area.areaname)
                    scores = self.model.get_area_scores(area.areaname, subject)
                    _scores = np.array(scores.all(), dtype="float32")
                    _scores.ravel()
                    if ignore0points:
                        _scores = _scores[_scores > 0]
                    else: _scores[_scores == 0] = 100
                    counts.append(_scores.size)
                    if _scores.size == 0: scores_list.append(0)
                    else: scores_list.append(round(np.average(_scores), 1))
                first = False
                scores_lists.append(scores_list)
                counts_list.append(counts)
            lists = [areas_list, scores_lists, counts_list]
            self.view.show_avg_score_diagramm(lists, subjects, 'Середній бал (200)', regname)
        else:
            print(f"No region with {regname} name!")

    def analyse_area_avg(self, areaname, subjects, ignore0points = True):
        area = self.model.get_area_by_name(areaname)
        if area:
            teritories = area.teritories
            scores_lists = []
            counts_list = []
            teritories_list = []
            first = True
            for subject in subjects:
                scores_list = []
                counts = []
                for ter in teritories:
                    if first: teritories_list.append(ter.tername)
                    scores = self.model.get_ter_scores(ter.tername, subject)
                    _scores = np.array(scores.all(), dtype="float32")
                    _scores.ravel()
                    if ignore0points:
                        _scores = _scores[_scores > 0]
                    else: _scores[_scores == 0] = 100
                    counts.append(_scores.size)
                    if _scores.size == 0: scores_list.append(0)
                    else: scores_list.append(round(np.average(_scores), 1))
                first = False
                scores_lists.append(scores_list)
                counts_list.append(counts)
            lists = [teritories_list, scores_lists, counts_list]
            self.view.show_avg_score_diagramm(lists, subjects, 'Середній бал (200)', areaname)
        else:
            print(f"No area with {areaname} name!")

    def analyse_Ukraine_failure_percentage(self, subjects):
        regions = self.model.get_all("regions")
        percent_lists = []
        regions_list = []
        counts_list = []
        first = True
        for subject in subjects:
            percent_list = []
            counts = []
            for region in regions:
                if first: regions_list.append(region.regname)
                scores = self.model.get_region_scores(region.regname, subject)
                _scores = np.array(scores.all(), dtype="float32")
                _scores.ravel()
                counts.append(_scores.size)
                if _scores.size == 0: percent_list.append(0)
                else: percent_list.append(round((_scores[_scores == 0].size/_scores.size)*100, 2))
            first = False
            percent_lists.append(percent_list)
            counts_list.append(counts)
        lists = [regions_list, percent_lists, counts_list]
        self.view.show_avg_score_diagramm(lists, subjects, fail_text, 'Україна')

    def analyse_region_failure_percentage(self, regname, subjects):
        region = self.model.get_region_by_name(regname)
        if region:
            areas = region.areas
            percent_lists = []
            areas_list = []
            counts_list = []
            first = True
            for subject in subjects:
                counts = []
                percent_list = []
                for area in areas:
                    if first: areas_list.append(area.areaname)
                    scores = self.model.get_area_scores(area.areaname, subject)
                    _scores = np.array(scores.all(), dtype="float32")
                    _scores.ravel()
                    counts.append(_scores.size)
                    if _scores.size == 0: percent_list.append(0)
                    else: percent_list.append(round((_scores[_scores == 0].size/_scores.size)*100, 2))
                first = False
                percent_lists.append(percent_list)
                counts_list.append(counts)
            lists = [areas_list, percent_lists, counts_list]
            self.view.show_avg_score_diagramm(lists, subjects,  fail_text, regname)
        else:
            print(f"No region with {regname} name!")

    def analyse_area_failure_percentage(self, areaname, subjects):
        area = self.model.get_area_by_name(areaname)
        if area:
            teritories = area.teritories
            percent_lists = []
            teritories_list = []
            counts_list = []
            first = True
            for subject in subjects:
                percent_list = []
                counts = []
                for ter in teritories:
                    if first: teritories_list.append(ter.tername)
                    scores = self.model.get_ter_scores(ter.tername, subject)
                    _scores = np.array(scores.all(), dtype="float32")
                    _scores.ravel()
                    counts.append(_scores.size)
                    if _scores.size == 0: percent_list.append(0)
                    else: percent_list.append(round((_scores[_scores == 0].size/_scores.size)*100, 2))
                first = False
                percent_lists.append(percent_list)
                counts_list.append(counts)
            lists = [teritories_list, percent_lists, counts_list]
            self.view.show_avg_score_diagramm(lists, subjects, fail_text, areaname)
        else:
            print(f"No area with {areaname} name!")
