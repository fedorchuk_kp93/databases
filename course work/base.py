from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os

main_engine = create_engine('postgresql://postgres:miahvbl0276@127.0.0.1:5432/ZNO_Statistic')
replica_engine = create_engine('postgresql://postgres:miahvbl0276@127.0.0.1:5435/ZNO_Statistic')
try:
    main_engine.connect()
    main_Session = sessionmaker(bind=main_engine)
    try:
        replica_engine.connect()
        replica_Session = sessionmaker(bind=replica_engine)
    except:
        replica_Session = main_Session
        print("Error: can't connect to replication server!")
except:
        print("Error: can't connect to PostgreSQL servers!")

Base = declarative_base()